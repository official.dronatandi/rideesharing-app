import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'base_api_services.dart';

class RequestTimeOut implements Exception {
  final String message;
  RequestTimeOut(this.message);
}

class InternetException implements Exception {
  final String message;
  InternetException(this.message);
}

class BadRequestException implements Exception {
  final String message;
  BadRequestException(this.message);
}

class UnauthorisedException implements Exception {
  final String message;
  UnauthorisedException(this.message);
}

class NotFoundException implements Exception {
  final String message;
  NotFoundException(this.message);
}

class ServerException implements Exception {
  final String message;
  ServerException(this.message);
}
//Authenticated Api Services
class ApiHelper {
  Future<dynamic> postApi(var data, String url, String jwtToken) async {
    if (kDebugMode) {
      print(url);
      print(data);
    }

    dynamic responseJson;

    try {
      final response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $jwtToken',
        },
        body: jsonEncode(data),
      );

      responseJson = returnResponse(response);
    } on SocketException {
      throw InternetException('No Internet connection');
    } on TimeoutException {
      throw RequestTimeOut('Request timed out');
    } catch (error) {
      // Handle other exceptions
      throw Exception('Error: $error');
    }

    if (kDebugMode) {
      print(responseJson);
    }

    return responseJson;
  }

  Future<dynamic> getApi(String url, String? jwtToken) async {
    if (kDebugMode) {
      print(url);
    }

    dynamic responseJson;

    try {
      final response = await http.get(
        Uri.parse(url),
        headers: {
          if (jwtToken != null) 'Authorization': 'Bearer $jwtToken',
          'Content-Type': 'application/json', // Include Content-Type header
        },
      );

      responseJson = returnResponse(response);

    } on SocketException {
      throw InternetException('No Internet connection');
    } on TimeoutException {
      throw RequestTimeOut('Request timed out');
    } catch (error) {
      // Handle other exceptions
      throw Exception('Error: $error');
    }

    print(responseJson);
    return responseJson;
  }

  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        return json.decode(response.body);
      case 201:
        return json.decode(response.body);
      case 204:
        return null; // No content
      case 400:
        throw BadRequestException(response.body);
      case 401:
      case 403:
        throw UnauthorisedException(response.body);
      case 404:
        throw NotFoundException(response.body);
      case 500:
        throw ServerException(response.body);
      default:
        throw Exception(
            'Failed to load data. Status code: ${response.statusCode}');
    }
  }
}

class NetworkApiServices extends BaseApiServices {
//un authentic api services
  @override
  Future<dynamic> getApi(String url) async {
    if (kDebugMode) {
      print(url);
    }

    dynamic responseJson;
    try {
      final response = await http.get(Uri.parse(url));
      responseJson = returnResponse(response);
    } on SocketException {
      throw InternetException('');
    } on RequestTimeOut {
      throw RequestTimeOut('');
    }
    print(responseJson);
    return responseJson;
  }

  @override
  Future<dynamic> postApi(var data, String url) async {
    if (kDebugMode) {
      print(url);
      print(data);
    }

    dynamic responseJson;
    try {
      final response = await http.post(Uri.parse(url),
          headers: {'Content-Type': 'application/json'},
          body: jsonEncode(data));
      responseJson = returnResponse(response);
    } on SocketException {
      throw InternetException('');
    } on RequestTimeOut {
      throw RequestTimeOut('');
    }
    if (kDebugMode) {
      print(responseJson);
    }
    return responseJson;
  }

  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 201:
        return json.decode(response.body);
      case 400:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;

      default:
        throw throw Exception(
            'Failed to load data. Status code: ${response.statusCode}');
    }
  }
}
