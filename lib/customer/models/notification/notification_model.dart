class NotificationModel {
  String? recipient_player_id;


  NotificationModel({this.recipient_player_id});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    recipient_player_id = json['recipient_player_id'];


  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['recipient_player_id'] = recipient_player_id;
    return data;
  }
}
