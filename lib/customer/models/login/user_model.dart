class UserModel {
  String? token;
  bool? isLogin ;
  int? userId;

  UserModel({this.token , this.isLogin, this.userId});

  UserModel.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    isLogin = json['isLogin'];
    userId = json['userId'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['token'] = token;
    data['isLogin'] = token;
    data['userId'] = userId;
    return data;
  }
}
