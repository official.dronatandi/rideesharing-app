class PartnerProfilesList {
  List<PartnerProfile>? partnerProfiles;

  PartnerProfilesList({this.partnerProfiles});

  PartnerProfilesList.fromJson(Map<String, dynamic> json) {
    if (json['partnerProfiles'] != null) {
      partnerProfiles = <PartnerProfile>[];
      json['partnerProfiles'].forEach((v) {
        partnerProfiles!.add(PartnerProfile.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (partnerProfiles != null) {
      data['partnerProfiles'] =
          partnerProfiles!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PartnerProfile {
  int? id;
  String? email;
  String? deviceId;
  String? userType;

  PartnerProfile({
    this.id,
    this.email,
    this.deviceId,
    this.userType,
  });

  PartnerProfile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    deviceId = json['deviceId'];
    userType = json['userType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['email'] = email;
    data['deviceId'] = deviceId;
    data['userType'] = userType;
    return data;
  }
}
