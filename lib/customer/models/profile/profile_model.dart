class UserProfile {
  int? id;
  String? email;
  String? firstname;
  String? lastname;
  String? imageUrl;
  String? bio;
  String? createdAt;
  String? updatedAt;
  int? userId;

  UserProfile(
      {this.id,
        this.email,
        this.firstname,
        this.lastname,
        this.imageUrl,
        this.bio,
        this.createdAt,
        this.updatedAt,
        this.userId});

  UserProfile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    imageUrl = json['imageUrl'];
    bio = json['bio'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['imageUrl'] = this.imageUrl;
    data['bio'] = this.bio;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['userId'] = this.userId;
    return data;
  }
}