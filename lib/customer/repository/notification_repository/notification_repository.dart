
import '../../models/login/user_model.dart' as CustomerUserModel;
import '../../data/network/network_api_services.dart';
import '../../models/notification/notification_model.dart';
import '../../res/app_url/app_url.dart';
import '../../view_models/controller/user_preference/user_prefrence_view_model.dart';

class NotificationRepository {
  final _apiService = ApiHelper();
  UserPreference userPreference = UserPreference();

  Future<dynamic> sendNotificationApi(NotificationModel notificationModel) async {
    CustomerUserModel.UserModel user = await userPreference.getUser();
    String token = user.token.toString();

    dynamic response = await _apiService.postApi(
        notificationModel, AppUrl.sendNotificationEndpoint, token);
    return response;
  }
}
