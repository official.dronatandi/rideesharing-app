
import '../../data/network/network_api_services.dart';
import '../../models/login/user_model.dart';
import '../../models/profile/profile_model.dart';
import '../../res/app_url/app_url.dart';
import '../../view_models/controller/user_preference/user_prefrence_view_model.dart';

class ProfileRepository {

  final _apiService  = ApiHelper() ;
  UserPreference userPreference = UserPreference();

  Future<dynamic> createAndUpdateProfileApi(UserProfile userProfile) async{
    UserModel user = await userPreference.getUser();
    String token = user.token.toString();

    dynamic response = await _apiService.postApi(userProfile, AppUrl.createAndUpdateProfileEndpoint,token);
    return response ;
  }

  Future<dynamic> getMyProfileApi() async{
    UserModel user = await userPreference.getUser();
    String token = user.token.toString();

    dynamic response = await _apiService.getApi(AppUrl.getMyProfileEndpoint,token);
    return response ;
  }


  Future<dynamic> getPartnerProfileApi() async{
    UserModel user = await userPreference.getUser();
    String token = user.token.toString();

    dynamic response = await _apiService.getApi(AppUrl.getPartnerProfileEndpoint,token);
    return response ;
  }





}