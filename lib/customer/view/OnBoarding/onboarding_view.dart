// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:ridesharing_app/customer/view/OnBoarding/page1.dart';
import 'package:ridesharing_app/customer/view/OnBoarding/page2.dart';
import 'package:ridesharing_app/customer/view/OnBoarding/page3.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../res/colors/app_color.dart';
import '../../res/dimen/dimen.dart';
import '../../res/routes/routes_name.dart';
import '../../res/strings/app_strings.dart';
import '../../res/style/style.dart';

class OnBoardingView extends StatefulWidget {
  const OnBoardingView({Key? key}) : super(key: key);

  @override
  State<OnBoardingView> createState() => _OnBoardingViewState();
}

class _OnBoardingViewState extends State<OnBoardingView> {
  final PageController _controller = PageController();
  bool isLastPage = false;
  bool isBack = false;

  List pages = [Page1(), Page2(), Page3()];

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: AppMargin.m20),
            alignment: Alignment.topRight,
            child: TextButton(
                onPressed: () => _controller.jumpToPage(pages.length),
                child: isLastPage
                    ? Text(
                        "",
                      )
                    : Text(
                        AppStrings.skip,
                        style: getRegularStyle(
                            fontSize: 18, color: AppColor.black),
                      )),
          ),
          SizedBox(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.8,
            child: PageView(
                controller: _controller,
                onPageChanged: (value) {
                  setState(() {
                    isLastPage = (value == 2);
                    isBack = value < 1;
                  });
                },
                children: List.generate(pages.length, (index) => pages[index])),
          ),
          isLastPage
              ? GestureDetector(
                onTap: ()=> Get.toNamed(RouteName.loginView),
                child: Container(
                    margin: EdgeInsets.symmetric(horizontal: AppPadding.p16),
                    alignment: Alignment.center,
                    width: double.infinity,
                    height: 50,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.amber,
                    ),
                    child: Text(
                      AppStrings.done,
                      style:
                          getRegularStyle(color: Colors.white, fontSize: 15),
                    ),
                  ),
              )
              : Container(
                  width: double.infinity,
                  height: 75,
                  padding: EdgeInsets.symmetric(horizontal: AppPadding.p16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SmoothPageIndicator(
                        controller: _controller,
                        count: 3,
                        effect: WormEffect(
                          dotWidth: 13,
                          dotHeight: 13,
                          activeDotColor: Colors.amber,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _controller.nextPage(
                              duration: Duration(milliseconds: 500),
                              curve: Curves.easeIn);
                          ///////////////////////////////
                        },
                        child: Container(
                          width: 100,
                          height: 50,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.amber,
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            AppStrings.next,
                            style: getRegularStyle(
                                color: Colors.white, fontSize: 15),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
        ],
      ),
    );
  }
}