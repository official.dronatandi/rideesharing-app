

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../res/components/round_button.dart';
import '../../../view_models/controller/login/login_view_model.dart';

class LoginButtonWidget extends StatelessWidget {
  final formKey ;
   LoginButtonWidget({Key? key , this.formKey}) : super(key: key);

  final loginVM = Get.put(LoginViewModel()) ;
  RxBool loading = false.obs;


  @override
  Widget build(BuildContext context) {
    return  Obx(() => RoundButton(
      buttonColor: Colors.amber,
        width: 300,
        title: 'login'.tr,
        loading: loginVM.loading.value,
        onPress: (){
          if(formKey.currentState!.validate()){
            loginVM.loginApi();
            loginVM.loading.value;
          }
        }
    ));
  }
}




