import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/utils.dart';
import '../../../view_models/controller/login/login_view_model.dart';

class InputPasswordWidget extends StatelessWidget {
  InputPasswordWidget({Key? key}) : super(key: key);

  final loginVM = Get.put(LoginViewModel());
  RxBool loading = false.obs;


  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: TextFormField(

        decoration: InputDecoration(
          hintText: 'Password',

          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: const BorderSide(color: Colors.black),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: const BorderSide(color: Colors.black)),

          hintStyle: const TextStyle(
            fontFamily: "Brand Bold",
            fontSize: 10,
          ),
          labelStyle: const TextStyle(
              fontFamily: "Brand Bold",
              color: Colors.grey,
              fontSize: 5
          ),

        ),

        controller: loginVM.passwordController.value,
        focusNode: loginVM.passwordFocusNode.value,
        obscureText: true,
        validator: (value) {
          if (value!.isEmpty) {
            Utils.snackBar('Password', 'Enter password');
          }
        },
        onFieldSubmitted: (value) {
        },
      )
    );
  }
}
