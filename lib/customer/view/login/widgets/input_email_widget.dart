import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/utils.dart';
import '../../../view_models/controller/login/login_view_model.dart';

class InputEmailWidget extends StatelessWidget {
  InputEmailWidget({Key? key}) : super(key: key);

  final loginVM = Get.put(LoginViewModel());
  RxBool loading = false.obs;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(horizontal: 10),
      child: TextFormField(
        decoration: InputDecoration(
          hintText: "Email",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: const BorderSide(color: Colors.black),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: const BorderSide(color: Colors.black)),
          hintStyle: const TextStyle(
            fontFamily: "Brand Bold",
            fontSize: 10,
          ),
          labelStyle: const TextStyle(
              fontFamily: "Brand Bold", color: Colors.grey, fontSize: 5),
        ),
        controller: loginVM.emailController.value,
        focusNode: loginVM.emailFocusNode.value,
        validator: (value) {
          if (value!.isEmpty) {
            Utils.snackBar('Email', 'Enter email');
          }
        },
        onFieldSubmitted: (value) {
          Utils.fieldFocusChange(
            context,
            loginVM.emailFocusNode.value,
            loginVM.passwordFocusNode.value,
          );
        },
      ),
    );
  }
}
