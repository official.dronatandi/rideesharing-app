import 'package:flutter/material.dart';
import '../../view/login/widgets/input_email_widget.dart';
import '../../view/login/widgets/input_password_widget.dart';
import '../../view/login/widgets/login_button_widget.dart';
import '../register/signup_view.dart';
import 'forgot_password_view.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {

 // final loginVM = Get.put(LoginViewModel()) ;
  final _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 100,
              ),
              const Image(
                image: AssetImage("assets/images/logo.png"),
                height: 150,
                width: 150,
                alignment: Alignment.topCenter,
              ),
              SizedBox(
                height: 20,
              ),
              const Text(
                "Login as a User",
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: "Brand Bold",
                ),
                textAlign: TextAlign.center,
              ),

              Form(
                key: _formkey,
                child: Column(
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.vertical(top: Radius.circular(20),),
                      ),
                      child: Column(
                        children: [
                          const SizedBox(height: 40,),
                          InputEmailWidget(),
                          const SizedBox(height: 20,),
                          InputPasswordWidget(),
                          SizedBox(height: size.height * 0.03),
                          Container(
                            alignment: Alignment.centerRight,
                            margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                            child: InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => const ForgetPasswordPage()));
                              },
                              child: const Text(
                                "Forgot your password?",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black
                                ),
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ],
                ),
              ),

              const SizedBox(height: 20,),
              LoginButtonWidget(formKey: _formkey,),
              SizedBox(height: size.height * 0.03),
              Container(
                alignment: Alignment.centerRight,
                margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: GestureDetector(
                  onTap: () => {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => SignupPage()))
                  },
                  child: const Text(
                    "Don't Have an Account? Sign up",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: Colors.black
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
