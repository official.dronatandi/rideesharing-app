import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ridesharing_app/customer/view/register/signup_button_widget.dart';
import '../../res/components/headingWidget.dart';
import '../login/login_view.dart';
import 'input_confirmPassword_widget.dart';
import 'input_email_widget.dart';
import 'input_password_widget.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({Key? key}) : super(key: key);

  @override
  State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {


  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xffEEF1F3),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const Image(
                  image: AssetImage("assets/images/logo.png"),
                  height: 150,
                  width: 150,
                  alignment: Alignment.topCenter,
                ),
                const PageHeading(title: 'Signup as a User',),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      //const PageHeader()

                      Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.vertical(top: Radius.circular(20),),
                        ),
                        child: Column(
                          children: [
                            EmailWidget(),
                            PasswordlWidget(),
                            ConfirmPasswordlWidget(),
                            const SizedBox(height: 22,),
                            SignupButtonWidget(formKey: _formKey,),
                            //LoginButtonWidget(formKey: _formKey),
                            const SizedBox(height: 18,),
                            SizedBox(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Text('Already have an account ? ', style: TextStyle(fontSize: 13, color: Colors.black, fontWeight: FontWeight.bold),),
                                  GestureDetector(
                                    onTap: () => {
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => const LoginView()))
                                    },
                                    child: const Text('Log-in', style: TextStyle(fontSize: 15, color: Colors.amber, fontWeight: FontWeight.bold),),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 30,),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _handleSignupUser() {
    // signup user
    if (_formKey.currentState!.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Submitting data..')),
      );
    }
  }
}