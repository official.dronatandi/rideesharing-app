import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../res/components/customFormWidget.dart';
import '../../view_models/auth_viewModel.dart';

class EmailWidget extends StatelessWidget {
  EmailWidget({Key? key}) : super(key: key);

  final mViewModel = Get.put(AuthViewModel());
  RxBool loading = false.obs;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomInputField(
          labelText: '',
          hintText: 'your email id',
          ctrl: mViewModel.emailController.value,
          validator: (textValue) {
            if (textValue == null || textValue.isEmpty) {
              return 'Email is required!';
            }
            if (!EmailValidator.validate(textValue)) {
              return 'Please enter a valid email';
            }
            return null;
          }),
    );
  }
}
