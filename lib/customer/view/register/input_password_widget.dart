import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../res/components/customFormWidget.dart';
import '../../utils/utils.dart';
import '../../view_models/auth_viewModel.dart';

class PasswordlWidget extends StatelessWidget {
  PasswordlWidget({Key? key}) : super(key: key);

  final mViewModel = Get.put(AuthViewModel());
  RxBool loading = false.obs;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomInputField(
          labelText: '',
          hintText: 'password',
          isDense: true,
          obscureText: true,
          ctrl: mViewModel.passwordController.value,
          focusNode: mViewModel.emailFocusNode.value,
          onFieldSubmitted: (value) {
            Utils.fieldFocusChange(
              context,
              mViewModel.passwordFocusNode.value,
              mViewModel.confirmPasswordFocusNode.value,
            );
          },
          suffixIcon: true,
          validator: (textValue) {
            if (textValue == null || textValue.isEmpty) {
              return 'Password is required!';
            }
            return null;
          }),
    );
  }
}
