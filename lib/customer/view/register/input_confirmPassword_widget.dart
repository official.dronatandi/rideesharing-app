import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../res/components/customFormWidget.dart';
import '../../view_models/auth_viewModel.dart';

class ConfirmPasswordlWidget extends StatelessWidget {
  ConfirmPasswordlWidget({Key? key}) : super(key: key);

  final mViewModel = Get.put(AuthViewModel());
  RxBool loading = false.obs;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomInputField(
          labelText: '',
          hintText: 'Confirm Password',
          isDense: true,
          obscureText: true,
          ctrl: mViewModel.confirmPasswordController.value,

          suffixIcon: true,
          validator: (textValue) {
            if (textValue == null || textValue.isEmpty) {
              return 'Password is required!';
            }
            return null;
          }),
    );
  }
}
