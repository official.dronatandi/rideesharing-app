import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../res/components/customFormWidget.dart';
import '../../utils/utils.dart';
import '../../view_models/auth_viewModel.dart';

class SignupButtonWidget extends StatelessWidget {
  final formKey;
  SignupButtonWidget({Key? key, this.formKey}) : super(key: key);

  final mViewModel = Get.put(AuthViewModel());
  RxBool loading = false.obs;

  @override
  Widget build(BuildContext context) {
    return Obx(() => CustomFormButton(
          loading: mViewModel.loading.value,
          innerText: 'Signup',
          onPressed: () {


            if(mViewModel.emailController.value.text.isEmpty
                || mViewModel.passwordController.value.text.isEmpty
                || mViewModel.confirmPasswordController.value.text.isEmpty

            ){
              Utils.snackBar('Input Error', 'All fields are required');
            }else{

              mViewModel.signupApi();
              mViewModel.loading.value;
              Utils.snackBar('Success', 'User created');

              mViewModel.emailController.value.clear();
              mViewModel.passwordController.value.clear();
              mViewModel.confirmPasswordController.value.clear();

            }

          }, formKey: formKey,
        ));
  }
}
