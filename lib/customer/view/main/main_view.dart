import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ridesharing_app/customer/view/main/setting/setting.dart';
import '../../models/profile/profile_model.dart';
import '../../res/routes/routes_name.dart';
import '../../res/strings/app_strings.dart';
import '../../view_models/splash_viewModel.dart';
import 'feed/feed_view.dart';
import 'landing_page_controller.dart';

class MainView extends StatelessWidget {

  final TextStyle unselectedLabelStyle = TextStyle(
      color: Colors.white.withOpacity(0.5),
      fontWeight: FontWeight.w500,
      fontSize: 12);

  final TextStyle selectedLabelStyle = const TextStyle(
      color: Colors.white, fontWeight: FontWeight.w500, fontSize: 12);

  MainView({super.key});



  buildBottomNavigationMenu(context, landingPageController) {
    return Obx(() => MediaQuery(
        data: MediaQuery.of(context)
            .copyWith(textScaler: const TextScaler.linear(1.0)),
        child: SizedBox(
          height: 56,
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            showUnselectedLabels: true,
            showSelectedLabels: true,
            onTap: landingPageController.changeTabIndex,
            currentIndex: landingPageController.tabIndex.value,
            backgroundColor: Colors.amber,
            unselectedItemColor: Colors.white.withOpacity(0.5),
            selectedItemColor: Colors.white,
            unselectedLabelStyle: unselectedLabelStyle,
            selectedLabelStyle: selectedLabelStyle,

            items: [
              BottomNavigationBarItem(
                icon: Container(
                  margin: const EdgeInsets.only(bottom: 7),
                  child: const Icon(
                    Icons.home,
                    size: 20.0,
                  ),
                ),
                label: 'Feed',
                backgroundColor: Colors.amber,
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: const EdgeInsets.only(bottom: 7),
                  child: const Icon(
                    Icons.settings,
                    size: 20.0,
                  ),
                ),
                label: 'Settings',
                backgroundColor: Colors.amber,
              ),
            ],
          ),
        )));
  }

  @override
  Widget build(BuildContext context) {
    final mViewModel  = Get.find<SplashViewModel>();

    // Fetch profile data when MainView is built
    mViewModel.fetchPrefProfileData();

    final LandingPageController landingPageController =
        Get.put(LandingPageController(), permanent: false);

    return SafeArea(
        child: PopScope(
      canPop: false,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 3,
          backgroundColor: Colors.amber,
          title: Obx(() {
            // Retrieve profile data from the SplashViewModel
            final UserProfile userProfile =
                mViewModel.prefUserProfile.value;

            // Check if userProfile is not null and first name is available
            if (userProfile != null && userProfile.firstname != null) {
              return RichText(
                text: TextSpan(
                  text: "Hello ${userProfile.firstname}",
                  style: const TextStyle(
                    fontSize: 17,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Brand Bold",
                  ),
                  children: const <TextSpan>[
                    TextSpan(
                      text: "",
                      style: TextStyle(
                        fontSize: 10,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Brand Bold",
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return const Text(
                "Hello User",
                style: TextStyle(
                  fontSize: 17,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontFamily: "Brand Bold",
                ),
              );
            }
          }),

          actions: [
            GestureDetector(
              onTap: () {
                Get.toNamed(RouteName.profileView);
              },
              child: ClipOval(
                child: Obx(() {
                  // Retrieve profile data from the SplashViewModel
                  final UserProfile userProfile =
                      mViewModel.prefUserProfile.value;

                  if (userProfile != null &&
                      userProfile.imageUrl!= null) {
                    return Image.network(
                      userProfile.imageUrl.toString(),
                      fit: BoxFit.cover,
                      width: 45.0,
                      height: 45.0,
                    );
                  } else {
                    // Return a placeholder if profile data is not available yet
                    return   ClipOval(
                      child: Image.network(
                        AppStrings.avatarUrl,
                        fit: BoxFit.cover,
                        width: 40.0,
                        height: 40.0,
                      ),
                    );
                  }
                }),
              ),
            ),
          ],
        ),
        backgroundColor: Colors.white,
        bottomNavigationBar:
            buildBottomNavigationMenu(context, landingPageController),
        body: Obx(() => IndexedStack(
              index: landingPageController.tabIndex.value,
              children:  [
                FeedView(),
                SettingView(),
              ],
            )),
      ),
    ));
  }
}
