import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../../res/routes/routes_name.dart';
import '../../../view_models/controller/user_preference/user_prefrence_view_model.dart';

class SettingView extends StatefulWidget {
  const SettingView({super.key});

  @override
  State<SettingView> createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  UserPreference userPreference = UserPreference();
  FirebaseAuth mAuth = FirebaseAuth.instance;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: const Color.fromRGBO(238, 240, 234, 1),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                height: 100,
                child: ListTile(
                  leading: const Padding(
                    padding: EdgeInsets.only(top: 0, bottom: 20),
                    child: SizedBox(
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                            "https://image.winudf.com/v2/image1/bmV0LndsbHBwci5ib3lzX3Byb2ZpbGVfcGljdHVyZXNfc2NyZWVuXzBfMTY2NzUzNzYxN18wOTk/screen-0.webp?fakeurl=1&type=.webp"),
                        backgroundColor: Colors.white,
                      ),
                    ),
                  ),
                  title: const Padding(
                    padding: EdgeInsets.only(top: 15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "User",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "GGGGGGGGGGGGG",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    setState(() {});
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Get.toNamed(RouteName.profileView)!.then((value) {});

                      },
                      child: AbsorbPointer(
                        child: SizedBox(
                          height: 50,
                          child: ListTile(
                            dense: true,
                            leading: Container(
                              height: 35,
                              width: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.deepOrangeAccent,
                              ),
                              child: const Icon(
                                Icons.person_outline,
                                color: Colors.white,
                              ),
                            ),
                            title: const Text(
                              'Profile',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                            trailing: const Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: Colors.black,
                              size: 15,
                            ),
                            onTap: () {
                              setState(() {});
                            },
                          ),
                        ),
                      ),
                    ),
                    const Divider(
                      thickness: 0,
                      color: Colors.grey,
                    ),
                    GestureDetector(
                      onTap: () {
                        //Navigator.pushNamed(context, '/profile');
                      },
                      child: AbsorbPointer(
                        child: SizedBox(
                          height: 50,
                          child: ListTile(
                            dense: true,
                            leading: Container(
                              height: 35,
                              width: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.deepOrangeAccent,
                              ),
                              child: const Icon(
                                Icons.person_outline,
                                color: Colors.white,
                              ),
                            ),
                            title: const Text(
                              'Demo',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                            trailing: const Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: Colors.black,
                              size: 15,
                            ),
                            onTap: () {
                              setState(() {});
                            },
                          ),
                        ),
                      ),
                    ),
                    const Divider(
                      thickness: 0,
                      color: Colors.grey,
                    ),
                    GestureDetector(
                      onTap: () {
                        //Navigator.pushNamed(context, '/profile');
                      },
                      child: AbsorbPointer(
                        child: SizedBox(
                          height: 50,
                          child: ListTile(
                            dense: true,
                            leading: Container(
                              height: 35,
                              width: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.deepOrangeAccent,
                              ),
                              child: const Icon(
                                Icons.person_outline,
                                color: Colors.white,
                              ),
                            ),
                            title: const Text(
                              'Demo',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                            trailing: const Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: Colors.black,
                              size: 15,
                            ),
                            onTap: () {
                              setState(() {});
                            },
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                child: GestureDetector(
                  onTap: () {
                    mAuth.signOut().then((_) {
                      userPreference.removeUserProfile();
                      userPreference.removeUser().then((_) {
                        Get.offNamed(RouteName.loginView);
                      }).catchError((error) {
                        // Handle error if necessary
                      });
                    }).catchError((error) {
                      // Handle error if necessary
                    });
                  },
                  child: AbsorbPointer(
                    child: SizedBox(
                      height: 50,
                      child: ListTile(
                        dense: true,
                        leading: const Icon(
                          Icons.logout,
                          color: Colors.red,
                        ),
                        title: const Text(
                          'Logout',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.red,
                          ),
                        ),
                        trailing: const Icon(
                          Icons.arrow_forward_ios_sharp,
                          color: Colors.black,
                          size: 15,
                        ),
                        onTap: () {
                          setState(() {});
                        },
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }
}
