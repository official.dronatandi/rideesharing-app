import 'package:flutter/material.dart';
import 'package:getwidget/components/carousel/gf_carousel.dart';

class Chips extends StatelessWidget {
  const Chips({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Chip(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
        backgroundColor: Colors.blueAccent,
        shadowColor: Colors.white,
        avatar: CircleAvatar(
          radius: 20,
          backgroundImage: NetworkImage(
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTqW0h8TJ1mmcr_VA9JUKqeel1F_EvdUmFXLulM7wuq82gRNzDcBEfzB9__KJnqgSPv4M&usqp=CAU"), //NetworkImage
        ), //CircleAvatar
        label: Text(
          'Chemistry',
          style: TextStyle(fontSize: 15,color: Colors.white),
        ), //Text
      ), //Chip
    ); //Center;
  }
}


class HeaderWidget extends StatelessWidget {
  final String text;

  HeaderWidget(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      color: Colors.white,
      child: Text(text,style: const TextStyle(fontSize: 18),),
    );
  }
}

class CategoryCard extends StatefulWidget {
  const CategoryCard({super.key});

  @override
  State<CategoryCard> createState() => _CategoryCardState();
}

class _CategoryCardState extends State<CategoryCard> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                padding: const EdgeInsets.all(5),
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(12),
                      topLeft: Radius.circular(12)),
                  border: Border.all(color: Colors.blueAccent),
                  color: Colors.white,
                ),
                child: const Icon(Icons.access_time_filled_sharp,
                    size: 50, color: Colors.blueAccent),
              ),
            ],
          ),
          Container(
            width: 100,
            decoration: const BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(12),
                    bottomLeft: Radius.circular(12))),
            child: Center(
                child: Text(
                  "Student",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                )),
            padding: const EdgeInsets.all(18),
          ),
        ],
      ),
    );

  }
}


class BodyWidget extends StatelessWidget {
  final Color color;

  BodyWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      color: color,
      alignment: Alignment.center,

    );
  }
}

class CarouselWidget extends StatefulWidget {
  const CarouselWidget({super.key});

  @override
  State<CarouselWidget> createState() => _CarouselWidgetState();
}

class _CarouselWidgetState extends State<CarouselWidget> {
  final List<String> imageList = [
    "https://cdn.pixabay.com/photo/2017/12/03/18/04/christmas-balls-2995437_960_720.jpg",
    "https://cdn.pixabay.com/photo/2017/12/13/00/23/christmas-3015776_960_720.jpg",
    "https://cdn.pixabay.com/photo/2019/12/19/10/55/christmas-market-4705877_960_720.jpg",
    "https://cdn.pixabay.com/photo/2019/12/20/00/03/road-4707345_960_720.jpg",
    "https://cdn.pixabay.com/photo/2019/12/22/04/18/x-mas-4711785__340.jpg",
    "https://cdn.pixabay.com/photo/2016/11/22/07/09/spruce-1848543__340.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
         width: MediaQuery.of(context).size.width,
    padding: const EdgeInsets.all(10),

    child: GFCarousel(
        items: imageList.map(
              (url) {
            return Container(
              margin: const EdgeInsets.all(8.0),
              child: ClipRRect(
                borderRadius:
                BorderRadius.all(Radius.circular(5.0)),
                child: Image.network(url,
                    fit: BoxFit.cover, width: 1000.0),
              ),
            );
          },
        ).toList(),
        onPageChanged: (index) {
          setState(() {
            index;
          });
        },
      ),
    );

  }
}

