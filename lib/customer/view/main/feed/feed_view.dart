import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../../../../customer/view_models/profile_viewModel.dart';
import '../../../view_models/notification_viewModel.dart';

class FeedView extends StatefulWidget {
  FeedView({Key? key}) : super(key: key);

  @override
  State<FeedView> createState() => _FeedViewState();
}

class _FeedViewState extends State<FeedView> {
  final mViewModel = Get.put(ProfileViewModel());
  final notificationViewModel = Get.put(NotificationViewModel());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber.withOpacity(0.1),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              onPressed: () {
                mViewModel.getPartnerProfileData();
              },
              child: Text("Fetch Partner Profiles"),
            ),
            Obx(() {
              if (mViewModel.loading.value) {
                return CircularProgressIndicator();
              } else {
                // Check if partnerProfileList is not null before accessing its properties
                if (mViewModel.partnerProfileList.value != null &&
                    mViewModel.partnerProfileList.value!.partnerProfiles != null) {
                  // Display partner profiles
                  return Expanded(
                    child: ListView.builder(
                      itemCount: mViewModel.partnerProfileList.value!.partnerProfiles!.length,
                      itemBuilder: (context, index) {
                        final partnerProfile = mViewModel.partnerProfileList.value!.partnerProfiles![index];
                        return  Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                            ),
                            height: 100,
                            child: ListTile(
                              leading: const Padding(
                                padding: EdgeInsets.only(top: 0, bottom: 20),
                                child: SizedBox(
                                  child: CircleAvatar(
                                    backgroundImage: NetworkImage(
                                        "https://image.winudf.com/v2/image1/bmV0LndsbHBwci5ib3lzX3Byb2ZpbGVfcGljdHVyZXNfc2NyZWVuXzBfMTY2NzUzNzYxN18wOTk/screen-0.webp?fakeurl=1&type=.webp"),
                                    backgroundColor: Colors.white,
                                  ),
                                ),
                              ),
                              title:  Padding(
                                padding: EdgeInsets.only(top: 15.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                         partnerProfile.email ?? '',
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'ID: ${partnerProfile.id}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                String? playerId = partnerProfile.deviceId;

                                notificationViewModel.sendNotificationApi(playerId);
                                print('Clicked partner ID: ${partnerProfile.id}');
                              },
                            ),
                          ),
                        );

                      },
                    ),
                  );
                } else {
                  // Handle case where partnerProfileList is null or empty
                  return Text('No partner profiles found.');
                }
              }
            }),
          ],
        ),
      ),
    );
  }
}
