import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomFormButton extends StatelessWidget {
  final String innerText;
  final bool loading;
  final formKey ;


  final void Function()? onPressed;
  const CustomFormButton({Key? key, required this.innerText, required this.onPressed,  this.loading = false,required this.formKey
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: Colors.amber,
        borderRadius: BorderRadius.circular(26),
      ),
      child: TextButton(
        onPressed: onPressed,
        child: loading ?
        const Center(child: CircularProgressIndicator(color: Colors.black,)) :
        Center(
          child: Text(innerText , style:  Theme.of(context).textTheme.titleMedium!.copyWith(color: Colors.white),),
        ),
      ),
    );
  }
}


class CustomInputField extends StatefulWidget {
  final TextEditingController? ctrl;
  final TextInputType? keyboardType;
  final FocusNode? focusNode;
  final Function? onFieldSubmitted;

  final String labelText;
  final String hintText;
  final String? Function(String?) validator;
  final bool suffixIcon;
  final bool? isDense;
  final bool obscureText;

  const CustomInputField({
    Key? key,
    this.ctrl,
    this.keyboardType,
    this.onFieldSubmitted,
    this.focusNode,
    required this.labelText,
    required this.hintText,
    required this.validator,
    this.suffixIcon = false,
    this.isDense,
    this.obscureText = false
  }) : super(key: key);

  @override
  State<CustomInputField> createState() => _CustomInputFieldState();
}

class _CustomInputFieldState extends State<CustomInputField> {
  //
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.9,
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 1),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(widget.labelText, style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold,color:Colors.black45 ),),
          ),
          TextFormField(
            controller: widget.ctrl,
            keyboardType: widget.keyboardType,
            focusNode: widget.focusNode,
            obscureText: (widget.obscureText && _obscureText),
            decoration: InputDecoration(

                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: const BorderSide(color: Colors.black),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: const BorderSide(color: Colors.black)),

              isDense: (widget.isDense != null) ? widget.isDense : false,
              hintText: widget.hintText,
              hintStyle: const TextStyle(
                fontFamily: "Brand Bold",
                fontSize: 10,
              ),
              labelStyle: const TextStyle(
                fontFamily: "Brand Bold",
                color: Colors.grey,
                fontSize: 5
              ),
              suffixIcon: widget.suffixIcon ? IconButton(
                icon: Icon(
                  _obscureText ? Icons.remove_red_eye : Icons.visibility_off_outlined,
                  color: Colors.black54,
                ),
                onPressed: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
              ): null,
              suffixIconConstraints: (widget.isDense != null) ? const BoxConstraints(
                  maxHeight: 33
              ): null,
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: widget.validator,
          ),
        ],
      ),
    );
  }
}
