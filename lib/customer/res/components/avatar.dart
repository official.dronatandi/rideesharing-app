import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    /*if ((user != '') || (user != null)) {
      return  const Hero(
        tag: 'User Avatar Image',
        child: SizedBox(
            width:4.0,
            height:40.0,
            child: Center(child:Text("DT",style: TextStyle(
              color: Colors.blue,
              fontSize: 20,
              fontWeight: FontWeight.bold,
              fontFamily: "Brand Bold"


            ),)),
            ),
      );
    }*/
    return Hero(
      tag: 'User Avatar Image',
      child: CircleAvatar(
          foregroundColor: Colors.blue,
          backgroundColor: Colors.white,
          radius: 40.0,
          child: ClipOval(
            child: Image.network(
              "https://cdn.pixabay.com/photo/2020/07/01/12/58/icon-5359553_1280.png",
              fit: BoxFit.cover,
              width: 40.0,
              height: 40.0,
            ),
          )),
    );
  }
}