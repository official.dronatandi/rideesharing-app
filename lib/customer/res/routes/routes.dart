

import 'package:get/get.dart';
import 'package:ridesharing_app/customer/res/routes/routes_name.dart';
import '../../view/OnBoarding/onboarding_view.dart';
import '../../view/login/login_view.dart';
import '../../view/main/charts/charts_view.dart';
import '../../view/main/main_view.dart';
import '../../view/register/signup_view.dart';
import '../../view/splash_screen.dart';


class AppRoutes {

  static appRoutes() => [
    GetPage(
      name: RouteName.splashScreen,
      page: () => SplashScreen() ,
      transitionDuration: const Duration(milliseconds: 250),
      transition: Transition.leftToRightWithFade ,
    ) ,
    GetPage(
      name: RouteName.loginView,
      page: () => const LoginView() ,
      transitionDuration: const Duration(milliseconds: 250),
      transition: Transition.leftToRightWithFade ,
    ) ,

    GetPage(
      name: RouteName.signupView,
      page: () => const SignupPage() ,
      transitionDuration: const Duration(milliseconds: 250),
      transition: Transition.leftToRightWithFade ,
    ) ,
    GetPage(
      name: RouteName.mainView,
      page: () => MainView() ,
      transitionDuration: const Duration(milliseconds: 250),
      transition: Transition.leftToRightWithFade ,
    ) ,
    GetPage(
      name: RouteName.onBoardingView,
      page: () => OnBoardingView() ,
      transitionDuration: const Duration(milliseconds: 250),
      transition: Transition.leftToRightWithFade ,
    ) ,

    GetPage(
      name: RouteName.chart,
      page: () => ChartsView() ,
      transitionDuration: const Duration(milliseconds: 250),
      transition: Transition.leftToRightWithFade ,
    ) ,

  ];

}