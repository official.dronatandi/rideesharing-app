

class AppUrl {

  static var baseUrl = "http://192.168.43.102:3000" ;

  //unAuthentic Urls
  static var loginApi =  '$baseUrl/auth/login' ;
  static var signupApi =  '$baseUrl/auth/signup' ;

  //Authentic Urls
  static var saveTransactionEndpoint =  '$baseUrl/api/transactions' ;
  static var createAndUpdateProfileEndpoint =  '$baseUrl/api/profile' ;
  static var getMyProfileEndpoint = '$baseUrl/api/getMyProfile' ;
  static var getPartnerProfileEndpoint = '$baseUrl/auth/getPartners' ;
  static var sendNotificationEndpoint = '$baseUrl/api/send-notification' ;




}