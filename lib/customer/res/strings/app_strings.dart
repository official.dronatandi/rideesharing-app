class AppStrings{
  static const String hello = 'Hello' ;
  static const noRouteFound = "noRouteFound";
  static const appName = "RTech Education";
  static const skip = "Skip";


  static const String next = "Next";
  static const String done = "Done";
  static const String login = "Login";
  static const String signUp = "Sign Up";
  static const String createAccount = "Create Account";
  static const String email = "Email";
  static const String password = "Password";
  static const String confirmPassword = "Confirm Password";
  static const String googleLogin = "Login with Google";
  static const String avatarUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-Ne7oVV6Lx9uAnmJDUZrrLcGy8yzo1sXdpQ&usqp=CAU";


  //Preferences String
  static const String isLogin = "isLogin";
  static const String token = "Login with Google";

}

class PreferencesString{
  static const String   userProfile = "userProfile";
  static const String isLogin = "isLogin";
  static const String token = "Login with Google";
  static const String firstname = "firstname";
  static const String lastname = "lastname";
  static const String email = "email";
  static const String imageUrl = "imageUrl";
  static const String userId = "userId";


}