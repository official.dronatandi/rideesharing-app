
import 'dart:ui';

class AppColor {

  static const Color blackColor = Color(0xff000000) ;
  static const Color whiteColor = Color(0xffffffff) ;

  static const Color primaryColor =  Color(0xff01B1C9);

  static const Color primaryButtonColor =  Color(0xff01B1C9);
  static const Color secondaryButtonColor =  Color(0xffFC3F5B);

  static const Color redColor =  Color(0xffFC3F5B);

  static const Color primaryTextColor =  Color(0xff000000);
  static const Color secondaryTextColor =  Color(0xff444648);
  static Color primary = const Color(0xff49CCD3);
  static Color lightPrimary = const Color(0xffDFF7F8); // color with 80% opacity
  static Color darkGrey = const Color(0xff525252);
  static Color grey = const Color(0xff737477);
  static Color lightGrey = const Color(0xffF5F5F5);
  static Color black = const Color(0xff000000);
  static Color white = const Color(0xffffffff);
  static Color error = const Color(0xffe10c0c);
  static Color card1 = const Color(0xffADEBED);
  static Color card2 = const Color(0xffF6BFB4);
  static Color card3 = const Color(0xffBDC0E1);


}