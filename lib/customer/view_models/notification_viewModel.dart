
import 'package:get/get.dart';
import '../models/notification/notification_model.dart';
import '../repository/notification_repository/notification_repository.dart';
import '../utils/utils.dart';
import 'controller/user_preference/user_prefrence_view_model.dart';

class NotificationViewModel extends GetxController {
  final _api = NotificationRepository();

  UserPreference userPreference = UserPreference();

  RxBool loading = false.obs;


  Future<void> sendNotificationApi(String? playerId) async {

    loading.value = true;

    NotificationModel notificationModel = NotificationModel(
      recipient_player_id: playerId,
    );

    try {
      await _api.sendNotificationApi(notificationModel);
      loading.value = false;
      Utils.snackBar('Success', "send notification successfully");
    } catch (error) {
      loading.value = false;
      Utils.snackBar('Error', error.toString());
    }
  }

}