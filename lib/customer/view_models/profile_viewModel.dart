import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import '../models/login/user_model.dart';
import '../models/profile/partner_model.dart';
import '../models/profile/profile_model.dart';
import '../repository/profile_repository/profile_repository.dart';
import '../utils/utils.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

import 'controller/user_preference/user_prefrence_view_model.dart';

class ProfileViewModel extends GetxController {
  final _api = ProfileRepository();

  final firstNameController = TextEditingController().obs;
  final lastnameController = TextEditingController().obs;
  final imageUrlController = RxString('');
  final emailController = TextEditingController().obs;
  final bioController = TextEditingController().obs;
  var previousImageUrl = RxString('');


  final firstNameNode = FocusNode().obs;
  final lastNameNode = FocusNode().obs;
  final imageUrlNode = FocusNode().obs;
  final bioNode = FocusNode().obs;

  RxBool loading = false.obs;
  RxBool isUpload = false.obs;

  var profileImage = Rx<File?>(null);
  UserPreference userPreference = UserPreference();


  @override
  void onInit() {
    super.onInit();
    // Load profile data when the ViewModel is initialized
    getProfileData();
    getPartnerProfileData();
  }

  @override
  void onClose() {
    profileImage.value = null;
    previousImageUrl.value = "";
    super.onClose();
  }

  Rx<UserProfile?> userProfile = UserProfile().obs;
  Rx<PartnerProfilesList?> partnerProfileList = PartnerProfilesList().obs;



  Future<void> getProfileData() async {
    loading.value = true;

    try {
      var responseData = await _api.getMyProfileApi();

      if (responseData != null && responseData['userProfile'] != null) {
        UserProfile profileData =
            UserProfile.fromJson(responseData['userProfile']);

        firstNameController.value.text = profileData.firstname ?? "";
        lastnameController.value.text = profileData.lastname ?? "";
        emailController.value.text = profileData.email ?? "";
        imageUrlController.value= profileData.imageUrl ?? "";
        bioController.value.text = profileData.bio ?? "";

        if (profileData.imageUrl != null && profileData.imageUrl!.isNotEmpty) {
          previousImageUrl.value = imageUrlController.value;
        }



      }

    } catch (error) {
      Utils.snackBar('Error', error.toString());
      log("error:$error");
    } finally {
      loading.value = false;
    }
  }


  Future<void> getPartnerProfileData1() async {
    loading.value = true;

    try {
      var responseData = await _api.getPartnerProfileApi();
      print('ResponseDatadrona: $responseData');

      if (responseData != null && responseData['partnerProfiles'] != null) {
        PartnerProfilesList partnerProfilesList = PartnerProfilesList.fromJson(responseData);

        if (partnerProfilesList.partnerProfiles != null && partnerProfilesList.partnerProfiles!.isNotEmpty) {
          partnerProfilesList.partnerProfiles!.forEach((partnerProfile) {
            print('Partner Email: ${partnerProfile.email}');
            print('Partner ID: ${partnerProfile.id}');
            // You can access other fields similarly
          });
        } else {
          // Handle case where partnerProfilesList is empty
          print('No partner profile data found in response');
        }
      } else {
        // Handle case where responseData is null or does not contain partner profile data
        print('Response data or partnerProfiles is null');
      }


    } catch (error) {
      Utils.snackBar('Error', error.toString());
      log("Error: $error");
    } finally {
      loading.value = false;
    }
  }

  Future<void> getPartnerProfileData() async {
    loading.value = true;

    try {
      var responseData = await _api.getPartnerProfileApi();

      if (responseData != null && responseData['partnerProfiles'] != null) {
        PartnerProfilesList partnerProfilesList = PartnerProfilesList.fromJson(responseData);

        if (partnerProfilesList.partnerProfiles != null && partnerProfilesList.partnerProfiles!.isNotEmpty) {
          // Store the fetched data in the observable
          this.partnerProfileList.value = partnerProfilesList;
        } else {
          print('No partner profile data found in response');
        }
      } else {
        print('Response data or partnerProfiles is null');
      }
    } catch (error) {
      Utils.snackBar('Error', error.toString());
      print("Error: $error");
    } finally {
      loading.value = false;
    }
  }



  void createAndUpdateProfile() {
    loading.value = true;

    // Create a ProfileModel instance with the provided data
    UserProfile userProfile = UserProfile(
      firstname: firstNameController.value.text,
      lastname: lastnameController.value.text,
      imageUrl: "https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png",
      bio: bioController.value.text,
    );

    _api.createAndUpdateProfileApi(userProfile).then((value) {
      loading.value = false;
      Utils.snackBar('Success', "Profile updated successfully");
    }).onError((error, stackTrace) {
      loading.value = false;
      Utils.snackBar('Error', error.toString());
    });
  }

  Future<void> pickImage() async {
    final pickedImage = await ImagePicker().pickImage(source: ImageSource.gallery);
    profileImage.value = pickedImage != null ? File(pickedImage.path) : null;
  }

  Future<void> uploadImage() async {
    UserModel user = await userPreference.getUser();
    String userId = user.userId.toString();
    await pickImage();

    if (profileImage.value == null) {
      print('No image picked.');
      return;
    }

    try {
      isUpload.value = true;

      final storageRef = firebase_storage.FirebaseStorage.instance
          .ref()
          .child('profile_images')
      .child(userId)
          .child("$userId.png");

      await storageRef.putFile(profileImage.value!);

      // Get the download URL
      final imageUrl = await storageRef.getDownloadURL();
      imageUrlController.value = imageUrl.tr;
      previousImageUrl.value = imageUrl.tr;

      // TODO: Save the imageUrl to your database or perform other actions
      print('Image uploaded. URL: $imageUrl');
    } catch (e) {
      print('Error uploading image: $e');
    } finally {
      isUpload.value = false;
    }
  }
}
