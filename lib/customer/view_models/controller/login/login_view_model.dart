import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../models/login/user_model.dart';
import '../../../repository/login_repository/login_repository.dart';
import '../../../res/routes/routes_name.dart';
import '../../../utils/utils.dart';
import '../user_preference/user_prefrence_view_model.dart';

class LoginViewModel extends GetxController {
  final _api = AuthRepository();

  UserPreference userPreference = UserPreference();

  final emailController = TextEditingController().obs;
  final passwordController = TextEditingController().obs;

  final emailFocusNode = FocusNode().obs;
  final passwordFocusNode = FocusNode().obs;

  RxBool loading = false.obs;
  FirebaseAuth mAuth = FirebaseAuth.instance;

  void loginApi() {
    loading.value = true;
    String email = emailController.value.text;
    String password = passwordController.value.text;

    if (email.isEmpty || password.isEmpty) {
      Utils.snackBar('Error', 'Email or password is required');
      loading.value = false;
      return;
    }

    mAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    ).then((value) {
      // Call login API
      Map data = {
        'email': email,
        'password': password,
      };
      _api.loginApi(data).then((value) {
        loading.value = false;

        if (value['error'] == 'user not found') {
          Utils.snackBar('Login', value['error']);
        } else {
          // Save user data and navigate to main view
          UserModel userModel = UserModel(
            token: value['token'],
            isLogin: true,
            userId: value['userId'],
          );
          userPreference.saveUser(userModel).then((value) {
            Get.delete<LoginViewModel>();
            Get.toNamed(RouteName.mainView);
          }).onError((error, stackTrace) {
            Utils.snackBar('Error', error.toString());
          });
        }
      }).onError((error, stackTrace) {
        loading.value = false;
        Utils.snackBar('Error', error.toString());
      });
    }).onError((error, stackTrace) {
      Utils.snackBar('Error', 'User does not exist in our system');
      loading.value = false;
    });
  }


}
