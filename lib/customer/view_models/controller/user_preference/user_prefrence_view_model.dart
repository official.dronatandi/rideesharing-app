

import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../models/login/user_model.dart';
import '../../../models/profile/profile_model.dart';
import '../../../res/strings/app_strings.dart';

class UserPreference {

    Future<bool> saveUser(UserModel responseModel)async{
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString('token', responseModel.token.toString());
      sp.setBool('isLogin', responseModel.isLogin!);
      sp.setInt('userId', responseModel.userId!);


      return true ;
    }

    Future<UserModel> getUser()async{
      SharedPreferences sp = await SharedPreferences.getInstance();
      String? token = sp.getString('token');
      bool? isLogin = sp.getBool('isLogin');
      int? userId = sp.getInt('userId');


      return UserModel(
        token: token ,
        isLogin: isLogin,
        userId: userId,
      ) ;
    }

    Future<bool> removeUser()async{
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.clear();
      return true ;
    }

    Future<bool> saveUserProfile(UserProfile userProfile) async {

      SharedPreferences sp = await SharedPreferences.getInstance();

      sp.setString(PreferencesString.firstname, userProfile.firstname ??"");
      sp.setString(PreferencesString.lastname, userProfile.lastname.toString());
      sp.setString(PreferencesString.email, userProfile.email.toString());
      sp.setString(PreferencesString.imageUrl, userProfile.imageUrl.toString());
      sp.setInt(PreferencesString.userId, userProfile.userId!);

      return true;
    }

    Future<bool> removeUserProfile()async{
      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.clear();
      return true ;
    }


    Future<UserProfile> getUserProfile()async{
      SharedPreferences sp = await SharedPreferences.getInstance();
      String? firstname = sp.getString('firstname');


      return UserProfile(
        firstname: firstname ,

      ) ;
    }
}