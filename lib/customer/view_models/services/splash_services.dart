

import 'dart:async';

import 'package:get/get.dart';

import '../../res/routes/routes_name.dart';
import '../controller/user_preference/user_prefrence_view_model.dart';
import '../splash_viewModel.dart';

class SplashServices {

  UserPreference userPreference = UserPreference();
  final mViewModel = Get.put(SplashViewModel()) ;


  void isLogin() {


    userPreference.getUser().then((value){

      print(value.token);
      print(value.isLogin);

      if(value.isLogin == false || value.isLogin.toString() == 'null'){
        Timer(const Duration(seconds: 3) ,
                () => Get.toNamed(RouteName.onBoardingView)
        );
      }else {
        mViewModel.getMyProfileData();
        Timer(const Duration(seconds: 3) ,
                () => Get.toNamed(RouteName.mainView) );
      }
    });

  }
}