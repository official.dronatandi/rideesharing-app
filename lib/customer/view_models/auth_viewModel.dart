import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../models/login/user_model.dart';
import '../repository/login_repository/login_repository.dart';
import '../res/routes/routes_name.dart';
import '../utils/utils.dart';
import 'controller/user_preference/user_prefrence_view_model.dart';

class AuthViewModel extends GetxController {
  final _api = AuthRepository();

  UserPreference userPreference = UserPreference();

  final emailController = TextEditingController().obs;
  final passwordController = TextEditingController().obs;
  final confirmPasswordController = TextEditingController().obs;

  final emailFocusNode = FocusNode().obs;
  final passwordFocusNode = FocusNode().obs;
  final confirmPasswordFocusNode = FocusNode().obs;

  RxBool loading = false.obs;

  FirebaseAuth mAuth = FirebaseAuth.instance;

  void signupApi() {
    loading.value = true;
    Map data = {
      'email': emailController.value.text,
      'password': passwordController.value.text,
      'userType':"customer"
    };

    mAuth
        .createUserWithEmailAndPassword(
            email: emailController.value.text.toString(),
            password: passwordController.value.text.toString())
        .then((value) {
      _api.signupApi(data).then((value) {
        loading.value = false;
        Get.toNamed(RouteName.loginView);
        Get.delete<AuthViewModel>();
      }).onError((error, stackTrace) {
        loading.value = false;
        Utils.snackBar('Error', error.toString());
      });
    }).onError((error, stackTrace) {
      Utils.snackBar('Error', "email already exist in our System");
      loading.value = false;

    });
  }
}
