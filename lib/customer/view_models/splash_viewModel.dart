import 'dart:convert';
import 'dart:developer';
import 'package:get/get.dart';
import '../models/profile/profile_model.dart';
import '../repository/profile_repository/profile_repository.dart';
import '../res/strings/app_strings.dart';
import '../utils/utils.dart';
import 'controller/user_preference/user_prefrence_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashViewModel extends GetxController {
  final _api = ProfileRepository();
  UserPreference userPreference = UserPreference();
  var prefUserProfile = UserProfile().obs;

  @override
  void onInit() {
    getMyProfileData();
    super.onInit();
    // Load profile data when the ViewModel is initialized
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> getMyProfileData() async {
    try {
      var responseData = await _api.getMyProfileApi();

      if (responseData != null &&
          responseData[PreferencesString.userProfile] != null) {

        UserProfile profileData =
        UserProfile.fromJson(responseData[PreferencesString.userProfile]);
        String profileDataJson = jsonEncode(profileData.toJson());

        // Save profile data to SharedPreferences
        SharedPreferences ps = await SharedPreferences.getInstance();
        await ps.setString(PreferencesString.userProfile, profileDataJson);
      }
    } catch (error) {
      Utils.snackBar('Error', error.toString());
      log("Error saving profile data: $error");
    }
  }

  Future<void> fetchPrefProfileData() async {
    try {
      SharedPreferences sp = await SharedPreferences.getInstance();
      String? userProfileJson = sp.getString(PreferencesString.userProfile);
      if (userProfileJson != null) {

        prefUserProfile.value =
            UserProfile.fromJson(json.decode(userProfileJson));
      }
    } catch (error) {
      Utils.snackBar('Error', error.toString());
      log("Error fetching profile data: $error");
    }
  }

}
